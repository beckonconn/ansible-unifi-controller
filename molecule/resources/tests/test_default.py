import pytest
import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


pytestmark = pytest.mark.filterwarnings('ignore::DeprecationWarning', 'ignore::PendingDeprecationWarning', 'ignore::ImportWarning')


@pytest.mark.parametrize('file, content', [('/etc/apt/sources.list.d/100-unifi.list', 'www.ubnt.com'), ('/etc/apt/trusted.gpg.d/unifi-repo.gpg', '')])
def test_repo_files_contents(host, file, content):
    file = host.file(file)
    assert file.exists
    if file == "/etc/apt/sources.list.d/100-unifi.list":
        assert file.contains(content)


@pytest.mark.parametrize('pkg', ['unifi', 'mongodb-server'])
def test_unifi_installed(host, pkg):
    pkg = host.package(pkg)
    assert pkg.is_installed


@pytest.mark.parametrize('ports', ['3478', '8080', '8443', '8880', '8843'])
def test_system_properies_file_is_set(host, ports):
    sysProp = host.file('/var/lib/unifi/system.properties')
    assert sysProp.exists
    assert sysProp.contains(ports)
